package com.vti.shoppera70backend.repository;

import com.vti.shoppera70backend.modal.entity.TokenActiveAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TokenRepository extends JpaRepository<TokenActiveAccount, Integer>, JpaSpecificationExecutor {
    TokenActiveAccount findByToken(String token);
}
