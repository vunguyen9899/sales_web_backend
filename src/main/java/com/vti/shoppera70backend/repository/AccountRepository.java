package com.vti.shoppera70backend.repository;

import com.vti.shoppera70backend.modal.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>, JpaSpecificationExecutor<Account> {

    Account findByUsername(String username);

    boolean existsByUsername(String username);
    List<Account> findByUsernameAndAddress(String username, String address);

    @Query( value = "from Account ac where ac.username = :username")

    List<Account> findByUsername2(String username);
}
