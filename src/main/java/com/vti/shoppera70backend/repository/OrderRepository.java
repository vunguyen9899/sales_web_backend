package com.vti.shoppera70backend.repository;

import com.vti.shoppera70backend.modal.Order;
import com.vti.shoppera70backend.modal.Product;
import com.vti.shoppera70backend.modal.dto.OrderCreateDto;
import com.vti.shoppera70backend.modal.dto.ProductCreateDto;
import com.vti.shoppera70backend.modal.entity.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer>{
//    List<Order> findAllByOrderStatusAndOrderBy_Id(OrderStatus orderStatus, int accountId);
    List<Order> findAllByOrderStatusAndOrderBy_Id(OrderStatus orderStatus, int accountId);
    List<Order> findAllByOrderBy_Id(int accountId);
}
