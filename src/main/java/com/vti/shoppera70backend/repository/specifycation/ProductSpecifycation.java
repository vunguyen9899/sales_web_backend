package com.vti.shoppera70backend.repository.specifycation;

import com.vti.shoppera70backend.modal.Product;
import com.vti.shoppera70backend.modal.dto.SearchAccountReq;
import com.vti.shoppera70backend.modal.dto.SearchProductReq;
import com.vti.shoppera70backend.modal.entity.Account;
import org.springframework.data.jpa.domain.Specification;

public class ProductSpecifycation {
    public static Specification<Product> buildCondition(SearchProductReq req) {
        return Specification.where(findByProductTypes(req))
                .and(findByShippingUnits(req))
                .and(findByProductStatuses(req))
                .and(findByProductName(req))
                .and(findByPrice(req));
    }

    private static Specification<Product> findByProductTypes (SearchProductReq req){
        if (req.getProductTypes() != null && req.getProductTypes().size() > 0) {
            return (root, query, cri) -> {
                return root.get("productType").in(req.getProductTypes());
            };
        } else {
            return null;
        }
    }
    private static Specification<Product> findByShippingUnits (SearchProductReq req){
        if (req.getShippingUnits() != null && req.getShippingUnits().size() > 0) {
            return (root, query, cri) -> {
                return root.get("shippingUnit").in(req.getShippingUnits());
            };
        } else {
            return null;
        }
    }
    private static Specification<Product> findByProductStatuses (SearchProductReq req){
        if (req.getProductStatuses() != null && req.getProductStatuses().size() > 0) {
            return (root, query, cri) -> {
                return root.get("productStatus").in(req.getProductStatuses());
            };
        } else {
            return null;
        }
    }
    private static Specification<Product> findByProductName (SearchProductReq req){
        if (req.getName() != null) {
            return (root, query, cri) -> {
                return cri.like(root.get("name"), "%" + req.getName() + "%");
            };
        } else {
            return null;
        }
    }
    private static Specification<Product> findByPrice (SearchProductReq req){
        if (req.getMaxPrice() != 0) {
            return (root, query, cri) -> {
                return cri.between(root.get("price"), req.getMinPrice(), req.getMaxPrice());
            };
        } else {
            return null;
        }
    }
}
