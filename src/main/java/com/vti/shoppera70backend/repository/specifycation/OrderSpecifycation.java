package com.vti.shoppera70backend.repository.specifycation;

import com.vti.shoppera70backend.modal.Order;
import com.vti.shoppera70backend.modal.Product;
import com.vti.shoppera70backend.modal.dto.SearchOrderReq;
import com.vti.shoppera70backend.modal.dto.SearchProductReq;
import org.springframework.data.jpa.domain.Specification;

public class OrderSpecifycation {
    public static Specification<Order> buildCondition(SearchOrderReq req){
        return Specification.where(findByOrderStatuses(req));
    }
    private static Specification<Order> findByOrderStatuses (SearchOrderReq req){
        if (req.getOrderStatuses() != null) {
            return (root, query, cri) -> {
//                return root.get("orderStatus").in(req.getOrderStatuses());
                return cri.equal(root.get("orderStatus"),  req.getOrderStatuses());
            };
        } else {
            return null;
        }
    }
}
