package com.vti.shoppera70backend.repository.specifycation;

import com.vti.shoppera70backend.modal.dto.SearchAccountReq;
import com.vti.shoppera70backend.modal.entity.Account;
import org.springframework.data.jpa.domain.Specification;

public class AccountSpecifycation {
    public static Specification<Account> buildCondition(SearchAccountReq request) {
        return Specification.where(findByUsername(request))
                .and(findByRole(request));
    }



    private static Specification<Account> findByUsername(SearchAccountReq request) {
        if (request.getUsername() != null) {
            // Tạo điều kiện tìm kiếm
            return (root, query, cri) -> {
        // root: Chọn cột, field, để tìm kiếm (giá trị là thuộc tính trong java)
        // cri: CriteriaBuilder Khai báo loại so sánh dữ liệu. ( lớn hơn, nhỏ hơn, equal, like,.... )
                return cri.like(root.get("username"), "%" + request.getUsername() + "%");
            };

        } else {
            return null;
        }
    }
    private static Specification<Account> findByRole(SearchAccountReq request) {
        if (request.getRole() != null) {
            return (root, query, cri) -> {
                return cri.equal(root.get("role"),  request.getRole());
            };
        } else {
            return null;
        }
    }
}