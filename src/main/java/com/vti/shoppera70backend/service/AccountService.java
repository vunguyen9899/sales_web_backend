package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.config.Exception.CustomerException;
import com.vti.shoppera70backend.config.Exception.ErrorResponseEnum;
import com.vti.shoppera70backend.modal.dto.AccountCreateDto;
import com.vti.shoppera70backend.modal.dto.AccountEditDto;
import com.vti.shoppera70backend.modal.dto.BaseReq;
import com.vti.shoppera70backend.modal.dto.SearchAccountReq;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.Role;
import com.vti.shoppera70backend.repository.AccountRepository;
import com.vti.shoppera70backend.repository.specifycation.AccountSpecifycation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service

public class AccountService implements IAccountService, UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Page<Account> search(SearchAccountReq req) {
        BaseReq.verify(req);

        Specification<Account> condition = AccountSpecifycation.buildCondition(req);

        PageRequest pageRequest = null;
        if ("DESC".equals(req.getSortType())){
            pageRequest = PageRequest.of(req.getPageNumber() -1, req.getPageSize(),
                    Sort.by(req.getSortField()).descending());
        }else {
            pageRequest = PageRequest.of(req.getPageNumber() -1, req.getPageSize(),
                    Sort.by(req.getSortField()).ascending());
        }

        return accountRepository.findAll(condition, pageRequest);
    }

    @Override
    public List<Account> getAll() {
//        SearchAccountReq req = new SearchAccountReq();
//        req.setRole(Role.CUSTOMER);
//        req.setUsername("o");
//        Specification<Account> condition = AccountSpecifycation.buildCondition(req);

        return accountRepository.findAll();
    }

    @Override
    public Account getById(int id) {

        Optional<Account> accountOptional = accountRepository.findById(id);

        if (accountOptional.isPresent()){
            return accountOptional.get();
        }
        return null;
    }

    @Override
    public Account findByUsername(String username) {
        return accountRepository.findByUsername(username);
    }

    @Override
    public Account create(AccountCreateDto dto) {
        if (accountRepository.existsByUsername(dto.getUsername())){
            throw new CustomerException(ErrorResponseEnum.USERNAME_EXISTED);
        }

        Account account = new Account();

        BeanUtils.copyProperties(dto, account);
        account.setRole(Role.CUSTOMER);
        account.setPassword(passwordEncoder.encode(dto.getPassword()));
        return accountRepository.save(account);
    }

    @Override
    public void delete(int id) {
        if (accountRepository.existsById(id)){
            accountRepository.deleteById(id);
        } else {
            throw new RuntimeException("id ko tồn tại");
        }
    }

    @Override
    public Account edit(AccountEditDto dto) {
        Account account =new Account();
        BeanUtils.copyProperties(dto, account);
        account.setRole(Role.CUSTOMER);
        return accountRepository.save(account);
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       Account account=accountRepository.findByUsername(username);
       if (account == null) {
           throw new UsernameNotFoundException("username khong ton tai");
       }
       List<GrantedAuthority> authorities = new ArrayList<>();
       authorities.add(account.getRole());
        return new User(account.getUsername(), account.getPassword(), authorities);
    }
}
