package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.modal.Product;
import com.vti.shoppera70backend.modal.dto.ProductCreateDto;
import com.vti.shoppera70backend.modal.dto.ProductEditDto;
import com.vti.shoppera70backend.modal.dto.SearchProductReq;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IProductService {
    List<Product> getAll();
    Page<Product> search(SearchProductReq req);
    Product getById(int id);
    Product create(ProductCreateDto productCreateDto);
    void delete(int id);
    Product edit(ProductEditDto productEditDto);
}
