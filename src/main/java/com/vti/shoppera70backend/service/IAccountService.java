package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.modal.dto.AccountCreateDto;
import com.vti.shoppera70backend.modal.dto.AccountEditDto;
import com.vti.shoppera70backend.modal.dto.SearchAccountReq;
import com.vti.shoppera70backend.modal.entity.Account;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IAccountService {

    Page<Account> search(SearchAccountReq req);
    List<Account> getAll();
    Account getById(int id);

    Account findByUsername(String username);

    Account create(AccountCreateDto dto);
    void delete(int id);
    Account edit(AccountEditDto dto);
}
