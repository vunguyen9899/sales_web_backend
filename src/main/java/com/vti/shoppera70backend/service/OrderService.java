package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.modal.Order;
import com.vti.shoppera70backend.modal.Product;
import com.vti.shoppera70backend.modal.dto.*;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.OrderStatus;
import com.vti.shoppera70backend.modal.entity.Role;
import com.vti.shoppera70backend.repository.OrderRepository;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService implements IOrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private IProductService productService;

    @Autowired
    private IAccountService accountService;

//    @Override
//    public Page<Order> search(SearchOrderReq req) {
//        BaseReq.verify(req);
//        Specification<Order> condition = OrderSpecifycation.buildCondition(req);
//
//        PageRequest pageRequest = BaseReq.buildPageRequest(req);
//
//        return orderRepository.findAll(condition,pageRequest);
//    }
//    @Override
//    public List<Order> getByOrderStatus(OrderStatus orderStatus, int accountId) {
//        if (orderStatus==null){
//            return orderRepository.findAllByOrderBy_Id(accountId);
//        }else {
//            return orderRepository.findAllByOrderStatusAndOrderBy_Id(orderStatus,accountId);
//        }
//    }

    @Override
    public List<Order> getByOrderStatus(OrderStatus orderStatus, int accountId) {
        if (orderStatus==null){
            return orderRepository.findAllByOrderBy_Id(accountId);
        }else {
            return orderRepository.findAllByOrderStatusAndOrderBy_Id(orderStatus,accountId);
        }
    }

    @Override
    public List<Order> getAll() {
    return orderRepository.findAll();

    }
    @Override
    public Order getById(int id) {
            Optional<Order> orderOptional = orderRepository.findById(id);

            if (orderOptional.isPresent()){
                return orderOptional.get();
            }
            return null;
        }


    @Override
    public Order create(OrderCreateDto orderCreateDto) {
        Account account = accountService.getById(orderCreateDto.getAccountId());
        Product product = productService.getById(orderCreateDto.getProductId());
        Order order = new Order();
        order.setOrderBy(account);
        order.setProduct(product);
        order.setQuantity(orderCreateDto.getQuantity());
        order.setOrderStatus(OrderStatus.PENDING);
        return orderRepository.save(order);
    }

    @Override
    public void delete(int id) {
        if (orderRepository.existsById(id)){
            orderRepository.deleteById(id);
        } else {
            throw new RuntimeException("id ko tồn tại");
        }
    }

    @Override
    public Order payOrder(OrderEditDto orderEditDto) {
        Account account = accountService.getById(orderEditDto.getAccountId());
        Product product = productService.getById(orderEditDto.getProductId());
        Order order = new Order();
        order.setOrderBy(account);
        order.setProduct(product);
        order.setQuantity(orderEditDto.getQuantity());
        order.setCreateAt(orderEditDto.getCreateAt());
        order.setOrderStatus(OrderStatus.DONE);

        BeanUtils.copyProperties(orderEditDto, order);
//        order.setOrderStatus(OrderStatus.DONE);
//        order.setCreateAt(orderEditDto.getCreateAt());
        return orderRepository.save(order);
    }

    @Override
    public Order cancelOrder(OrderEditDto orderEditDto) {
        Account account = accountService.getById(orderEditDto.getAccountId());
        Product product = productService.getById(orderEditDto.getProductId());
        Order order = new Order();
        order.setOrderBy(account);
        order.setProduct(product);
        order.setQuantity(orderEditDto.getQuantity());


        BeanUtils.copyProperties(orderEditDto, order);
        order.setOrderStatus(OrderStatus.CANCEL);
        return orderRepository.save(order);
    }
}
