package com.vti.shoppera70backend.service;

import com.alibaba.fastjson.JSON;
import com.vti.shoppera70backend.modal.dto.AppExceptionDto;
import com.vti.shoppera70backend.modal.dto.LoginDto;
import com.vti.shoppera70backend.modal.entity.Role;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
@Slf4j
@Component
public class JWTTokenUtils {
    private static final long EXPIRATION_TIME = 864000000;
    private static final String SECRET = "123456";
    private static final String PREFIX_TOKEN = "Bearer";
//    private static final String AUTHORIZATION = "Authorization";
    private static final String AUTHORIZATION = "Authorization";
//    private static final com.alibaba.fastjson.JSON JSON = ;



    public String createAccessToken(LoginDto loginDto) {

        Date expirationDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
        return Jwts.builder()
                .setId(String.valueOf(loginDto.getId()))
                .setSubject(loginDto.getUsername())
                .setIssuedAt(new Date())
                .setIssuer("VTI")
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .claim("isActive", loginDto.isActive())
                .claim("authorities", loginDto.getRole().name())
                .claim("user-Agent", loginDto.getUserAgent()).compact();
    }


    public LoginDto parseAccessToken(String token) {
        LoginDto loginDto = new LoginDto();
        if (!token.isEmpty()) {
            try {
                token = token.replace(PREFIX_TOKEN, "").trim();
                Claims claims = Jwts.parser()
                        .setSigningKey(SECRET)
                        .parseClaimsJws(token).getBody();

                String user = claims.getSubject();
                Role role = Role.valueOf(claims.get("authorities").toString());
                boolean isActive = Boolean.parseBoolean(claims.get("isActive").toString());
//                String userAgent = claims.get("user-Agent").toString();

                loginDto.setUsername(user);
                loginDto.setRole(role);
                loginDto.setActive(isActive);
//                loginDto.setUserAgent(userAgent);
                loginDto.setId(Integer.parseInt(claims.getId()));
            } catch (Exception e) {
                log.error(e.getMessage());
                return null;
            }
        }
        return loginDto;
    }


    public LoginDto checkToken(String token, HttpServletResponse response, HttpServletRequest httpServletRequest) {
        LoginDto loginDto = null;
        try {
            if (StringUtils.isBlank(token) || !token.startsWith(PREFIX_TOKEN)) {
                responseJson(response, new AppExceptionDto("Token ko hợp lệ", 401, httpServletRequest.getRequestURI()));
            return null;
            }
            token = token.replace(PREFIX_TOKEN, "").trim();
            loginDto = parseAccessToken(token);
            if (loginDto == null) {
                responseJson(response, new AppExceptionDto("Token ko tồn tại hoặc hết hạn", 401, httpServletRequest.getRequestURI()));
                return null;
            }
            assert loginDto != null;
            if (!loginDto.isActive()){
                responseJson(response, new AppExceptionDto("chu kich hoat", 401, httpServletRequest.getRequestURI()));
                return null;
            }
        } catch (Exception e) {
            responseJson(response, new AppExceptionDto(e.getMessage(), 401, httpServletRequest.getRequestURI()));
        }
        return loginDto;
    }

    private void responseJson(HttpServletResponse response, AppExceptionDto appException) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.setStatus(appException.getStatus());
        try {
            response.getWriter().print(JSON.toJSONString(appException));
        } catch (IOException e) {
            log.debug(e.getMessage());
            throw new RuntimeException(e);
        }
    }
}








