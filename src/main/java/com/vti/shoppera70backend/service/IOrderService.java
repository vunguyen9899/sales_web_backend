package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.modal.Order;
import com.vti.shoppera70backend.modal.dto.OrderCreateDto;
import com.vti.shoppera70backend.modal.dto.OrderEditDto;
import com.vti.shoppera70backend.modal.dto.SearchAccountReq;
import com.vti.shoppera70backend.modal.dto.SearchOrderReq;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.OrderStatus;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IOrderService {
//    Page<Order> search(SearchOrderReq req);
    List<Order> getByOrderStatus(OrderStatus orderStatus, int accountId);
    List<Order> getAll();
    Order getById(int id);
    Order create(OrderCreateDto orderCreateDto);
    void delete(int id);
    Order payOrder(OrderEditDto orderEditDto);
    Order cancelOrder(OrderEditDto orderEditDto);
}
