package com.vti.shoppera70backend.service;

import com.vti.shoppera70backend.config.Exception.CustomerException;
import com.vti.shoppera70backend.config.Exception.ErrorResponseEnum;
import com.vti.shoppera70backend.modal.Product;
import com.vti.shoppera70backend.modal.dto.BaseReq;
import com.vti.shoppera70backend.modal.dto.ProductCreateDto;
import com.vti.shoppera70backend.modal.dto.ProductEditDto;
import com.vti.shoppera70backend.modal.dto.SearchProductReq;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.Role;
import com.vti.shoppera70backend.repository.ProductRepository;
import com.vti.shoppera70backend.repository.specifycation.ProductSpecifycation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackOn = Exception.class)
public class ProductService implements IProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    public Page<Product> search(SearchProductReq req) {
        BaseReq.verify(req);
        Specification<Product> condition = ProductSpecifycation.buildCondition(req);

        PageRequest pageRequest = BaseReq.buildPageRequest(req);

        return productRepository.findAll(condition,pageRequest);
    }

    @Override
    public Product getById(int id) {
        Optional<Product> productOptional = productRepository.findById(id);

        if (productOptional.isEmpty()){
            throw new CustomerException(ErrorResponseEnum.NOT_FOUND_PRODUCT);
        }
        return productOptional.get();
    }

    @Override
    public Product create(ProductCreateDto productCreateDto) {
        Product product = new Product();

        BeanUtils.copyProperties(productCreateDto, product);
        return productRepository.save(product);

    }

    @Override
    public void delete(int id) {
        if (productRepository.existsById(id)){
            productRepository.deleteById(id);
        } else {
            throw new RuntimeException("id ko tồn tại");
        }
    }

    @Override
    public Product edit(ProductEditDto productEditDto) {
        Product product = new Product();
        BeanUtils.copyProperties(productEditDto, product);
        return productRepository.save(product);

    }
}
