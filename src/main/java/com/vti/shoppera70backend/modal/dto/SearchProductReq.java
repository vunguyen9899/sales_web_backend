package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.ProductStatus;
import com.vti.shoppera70backend.modal.entity.ProductType;
import com.vti.shoppera70backend.modal.entity.ShippingUnit;
import lombok.Data;

import java.util.List;

@Data
public class SearchProductReq extends BaseReq {
    private List<ProductStatus> productStatuses;
    private List<ShippingUnit> shippingUnits;
    private List<ProductType> productTypes;
    private int minPrice;
    private int maxPrice;
    private String name;
}
