package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.Product;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.OrderStatus;
import lombok.Data;

import javax.persistence.ManyToOne;
import java.util.Date;

@Data
public class OrderEditDto {
    private int id;
    private OrderStatus orderStatus;
    private int quantity;
    private int productId;
    private int accountId;
    protected Date createAt;
}
