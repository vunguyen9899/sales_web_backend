package com.vti.shoppera70backend.modal.dto;

import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Data
public class BaseReq {
    protected int pageNumber;
    protected int pageSize;
    protected String sortField;
    protected String sortType;

    public static void verify(BaseReq req) {
        if (req.getPageNumber() == 0) {
            req.setPageNumber(1);
        }
        if (req.getPageSize() == 0) {
            req.setPageSize(5);
        }
        if (req.getSortField() == null || "".equals(req.getSortField())) {
            req.setSortField("id");
        }
        if (req.getSortType() == null || "".equals(req.getSortType())) {
            req.setSortType("DESC");
        }
    }

    public static PageRequest buildPageRequest(BaseReq req) {
        PageRequest pageRequest = null;
        if ("DESC".equals(req.getSortType())) {
            pageRequest = PageRequest.of(req.getPageNumber() - 1, req.getPageSize(),
                    Sort.by(req.getSortField()).descending());
        } else {
            pageRequest = PageRequest.of(req.getPageNumber() - 1, req.getPageSize(),
                    Sort.by(req.getSortField()).ascending());
        }
        return pageRequest;
    }
}
