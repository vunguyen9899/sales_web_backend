package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountEditDto {
    private int id;
    private String username;
    private Role role;
    private String phoneNumber;
    private String password;
    private String information;
    private String fullName;
    private String email;
    private Date dateofbirth;
    private String address;
}
