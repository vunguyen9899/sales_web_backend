package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.OrderStatus;
import lombok.Data;

import java.util.List;

@Data
public class SearchOrderReq extends BaseReq {
    private OrderStatus orderStatuses;
}
