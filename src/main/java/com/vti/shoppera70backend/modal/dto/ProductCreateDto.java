package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.ProductStatus;
import com.vti.shoppera70backend.modal.entity.ProductType;
import com.vti.shoppera70backend.modal.entity.ShippingUnit;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductCreateDto {

    private ProductStatus productStatus;
    private ShippingUnit shippingUnit;
    private ProductType productType;
    private int price;
    private String image;
    @Length(min = 1, max = 255, message = "do dai name phai tu 1-255")
    @NotNull
    private String name;
}
