package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.Role;
import lombok.Data;

@Data
public class LoginDto {

    private int id;
    private String username;
    private Role role;
    private String fullName;
    private String token;
    private boolean isActive;
    private String userAgent;
}
