package com.vti.shoppera70backend.modal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppExceptionDto {
    private Instant timestamp;
    private String message;
    private String path;
    private int status;

    public AppExceptionDto(String message, int status, String path) {
        this.status = status;
        this.message = message;
        this.path = path;
        this.timestamp = Instant.now();
    }

}
