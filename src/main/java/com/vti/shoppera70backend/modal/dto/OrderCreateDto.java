package com.vti.shoppera70backend.modal.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrderCreateDto {
    private int quantity;
    private int productId;
    private int accountId;
}
