package com.vti.shoppera70backend.modal.dto;

import com.vti.shoppera70backend.modal.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountCreateDto {
    @NotBlank
    private String username;
    private Role role;
    private String phoneNumber;
    @NotBlank
    private String password;
    private String information;
    private String fullName;
    @NotBlank
    private String email;
    private Date dateofbirth;
    private String address;
}


