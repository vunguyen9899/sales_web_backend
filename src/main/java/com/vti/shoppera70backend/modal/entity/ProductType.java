package com.vti.shoppera70backend.modal.entity;

public enum ProductType {
    CHAINSAWS, BRUSHCUTTERS, ACCESSORIES, OIL
}
