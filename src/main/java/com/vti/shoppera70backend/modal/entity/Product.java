package com.vti.shoppera70backend.modal;

import com.vti.shoppera70backend.modal.entity.EntityBase;
import com.vti.shoppera70backend.modal.entity.ProductStatus;
import com.vti.shoppera70backend.modal.entity.ProductType;
import com.vti.shoppera70backend.modal.entity.ShippingUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Product")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "product_status")
    private ProductStatus productStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "shipping_unit")
    private ShippingUnit shippingUnit;

    @Enumerated(EnumType.STRING)
    @Column(name = "product_type")
    private ProductType productType;

    @Column(name = "price")
    private int price;
    @Column(name = "name")
    private String name;

    @Column(name = "image")
    private String image;
}
