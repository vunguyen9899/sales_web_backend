package com.vti.shoppera70backend.modal.entity;

public enum ProductStatus {
    OLD, NEW
}
