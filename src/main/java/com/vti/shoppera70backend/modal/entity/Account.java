package com.vti.shoppera70backend.modal.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Date;

@Entity
@Table(name = "`Account`")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Account extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "USERNAME", length = 50, unique = true, nullable = false)
    private String username;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "PHONE_NUMBER", length = 12, unique = true)
    private String phoneNumber;

    @Column(name = "PASSWORD", length = 100, nullable = false)
    private String password;

    @Column(name = "INFORMATION", length = 50)
    private String information;

    @Column(name = "FULL_NAME", length = 100)
    private String fullName;

    @Column(name = "EMAIL", length = 100, unique = true, nullable = false)
    private String email;

    @Column(name = "DATE_OF_BIRTH")
    private Date dateofbirth;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "IS_ACTIVE")
    private boolean isActive;

}
