package com.vti.shoppera70backend.modal.entity;

public enum ShippingUnit {
    EXPRESS, FAST, SAVE
}
