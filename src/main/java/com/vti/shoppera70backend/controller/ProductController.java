package com.vti.shoppera70backend.controller;

import com.vti.shoppera70backend.config.Exception.CustomerException;
import com.vti.shoppera70backend.modal.Product;
import com.vti.shoppera70backend.modal.dto.*;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.service.IProductService;
import com.vti.shoppera70backend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping(value = "/api/v1/product")
@CrossOrigin(value = "*")
public class ProductController {
    @Autowired
    private IProductService productService;
    @PostMapping("/search")
    public Page<Product> search(@RequestBody SearchProductReq req){
        return productService.search(req);
    }
    @GetMapping("/get-all")
    public List<Product> getAll(){
        return productService.getAll();
    }

//    @GetMapping("/{id}")
//    public ResponseEntity<Product> findByID(@PathVariable int id){
////        try {
////            productService.getById(id);
////            return productService.getById(id);
////        } catch (Exception e){
////            return e.getMessage();
////        }
////        try{
////            ResponseEntity.ok(productService.getById(id));
////        } catch (Exception e){
////            CustomerException customerException= new CustomerException();
////        }
//
//}
    @GetMapping("/{id}")
    public Product findByID(@PathVariable int id){
        return productService.getById(id);
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public String delete(@PathVariable int id){

            productService.delete(id);
            return "delete success";

    }
    @PostMapping("/create")
    public Product create(@RequestBody @Valid ProductCreateDto productCreateDto){
        return productService.create(productCreateDto);
    }
    @PutMapping("/edit")
    public Product edit(@RequestBody ProductEditDto productEditDto ){
        return productService.edit(productEditDto);
    }
}
