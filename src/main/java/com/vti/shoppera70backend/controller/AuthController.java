package com.vti.shoppera70backend.controller;

import com.vti.shoppera70backend.config.Exception.CustomerException;
import com.vti.shoppera70backend.config.Exception.ErrorResponseEnum;
import com.vti.shoppera70backend.modal.dto.AccountCreateDto;
import com.vti.shoppera70backend.modal.dto.LoginDto;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.modal.entity.Role;
import com.vti.shoppera70backend.modal.entity.TokenActiveAccount;
import com.vti.shoppera70backend.repository.AccountRepository;
import com.vti.shoppera70backend.repository.TokenRepository;
import com.vti.shoppera70backend.service.IMailSenderService;
import com.vti.shoppera70backend.service.JWTTokenUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/v1/auth")
@CrossOrigin(value = "*")
public class AuthController {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JWTTokenUtils jwtTokenUtils;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private IMailSenderService iMailSenderService;

    @PostMapping("/login-v1")// api phải qua bước authen mới vào đây
    public LoginDto LoginBasicV1(Principal principal){
        // principal: đc sinh ra tại bước authen
        String username = principal.getName();
        Account account = accountRepository.findByUsername(username);

        LoginDto loginDto = new LoginDto();
        BeanUtils.copyProperties(account, loginDto);
        return loginDto;
    }

    @PostMapping("/login-v2")
    public LoginDto LoginBasicV2(@RequestParam String username, @RequestParam String password){
            // để kt được logic username và password > cần public api này
            // principal: đc sinh ra tai bước authen

            Account account = accountRepository.findByUsername(username);
            if (account==null){
                throw new CustomerException(ErrorResponseEnum.LOGIN_FAIL_USERNAME);
            }
            if (!passwordEncoder.matches(password, account.getPassword())){
                throw  new CustomerException((ErrorResponseEnum.LOGIN_FAIL_PASSWORD));
            }
            LoginDto loginDto = new LoginDto();
            BeanUtils.copyProperties(account, loginDto);
            return loginDto;
    }
// jwt(required = false, name = "password")
    @PostMapping("/login-jwt")
    public LoginDto Loginjwt(@RequestParam String username, @RequestParam String password){

        Account account = accountRepository.findByUsername(username);
        if (account==null){
            throw new CustomerException(ErrorResponseEnum.LOGIN_FAIL_USERNAME);
        }
        if (!passwordEncoder.matches(password, account.getPassword())){
            throw  new CustomerException(ErrorResponseEnum.LOGIN_FAIL_PASSWORD);
        }
        if (!account.isActive()){
            throw new CustomerException(ErrorResponseEnum.NOT_FOUND_ACCOUNT);
        }
        LoginDto loginDto = new LoginDto();
        BeanUtils.copyProperties(account,loginDto);
        loginDto.setActive(account.isActive());

        String token = jwtTokenUtils.createAccessToken(loginDto);
        loginDto.setToken(token);
        return loginDto;
    }

    @GetMapping("/active/{token}")
    public String activeAccount(@PathVariable String token){
        TokenActiveAccount tokenActiveAccount = tokenRepository.findByToken(token);
        if (tokenActiveAccount == null){
            return "tài khoản không tồn tại";
        }
        Account account = tokenActiveAccount.getAccount();
        if (account.isActive()){
            return "tài khoản đã được kích hoạt";
        }
        account.setActive(true);
        accountRepository.save(account);
        return "tài khoản đã được kích hoạt";
    }

    @PostMapping("/register")
    public Account create(@RequestBody AccountCreateDto accountCreateDto){
        Account account = new Account();
        BeanUtils.copyProperties(accountCreateDto, account);


        account.setActive(false);
        account.setRole(Role.CUSTOMER);
        account.setPassword(passwordEncoder.encode(accountCreateDto.getPassword()));
        account = accountRepository.save(account);

        String token = UUID.randomUUID().toString();
        TokenActiveAccount tokenActiveAccount = new TokenActiveAccount();
        tokenActiveAccount.setAccount(account);
        tokenActiveAccount.setToken(token);
        tokenRepository.save(tokenActiveAccount);

        String text = "<div> :<a href=\"http://127.0.0.1:8080/api/v1/auth/active/"+token+"\">click</a></div>  ";
        iMailSenderService.sendMessageWithAttachment(account.getEmail(), "đăng kí tài khoản thành công", text );
        return account;
    }
}
