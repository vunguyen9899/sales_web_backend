package com.vti.shoppera70backend.controller;

import com.vti.shoppera70backend.modal.Order;
import com.vti.shoppera70backend.modal.dto.*;
import com.vti.shoppera70backend.modal.entity.OrderStatus;
import com.vti.shoppera70backend.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/order")
@CrossOrigin(value = "*")
public class OrderController {
    @Autowired
    private IOrderService orderService;
//    @PostMapping("/search")
//    public Page<Order> search(@RequestBody SearchOrderReq req){
//        return orderService.search(req);
//    }
    @PostMapping("/get-all")//api: localhost:8080/api/v1/account/get-all
    public List<Order> getAll(){
        return orderService.getAll();
    }

    @GetMapping("/{id}")
    public Order getById(@PathVariable int id){
        return orderService.getById(id);
    }

    @PostMapping("/create")
    public Order create(@RequestBody OrderCreateDto orderCreateDto){
        return orderService.create(orderCreateDto);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable int id){
        orderService.delete(id);
        return "Xoá thành công";
    }
    @PostMapping ("/get-by-orderstatus")
    public List<Order> getByOrderStatus(@RequestParam(required = false) OrderStatus orderStatus,@RequestParam(value = "accountId") Integer accountId){
        return orderService.getByOrderStatus(orderStatus, accountId);
    }
    @PutMapping("/pay")
    public Order payOrder(@RequestBody OrderEditDto orderEditDto){
        return orderService.payOrder(orderEditDto);
    }
    @PutMapping("/cancel")
    public Order cancelOrder(@RequestBody OrderEditDto orderEditDto){

        return orderService.cancelOrder(orderEditDto);
    }
}
