package com.vti.shoppera70backend.controller;

import com.vti.shoppera70backend.modal.dto.AccountCreateDto;
import com.vti.shoppera70backend.modal.dto.AccountEditDto;
import com.vti.shoppera70backend.modal.dto.SearchAccountReq;
import com.vti.shoppera70backend.modal.entity.Account;
import com.vti.shoppera70backend.service.AccountService;
import com.vti.shoppera70backend.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController // rest full api
@RequestMapping(value = "/api/v1/account")
@CrossOrigin(value = "*")
public class AccountController {

    @Autowired
    private IAccountService accountService;

//    private final AccountService accountService;
//    @Autowired
//    public AccountController(AccountService accountService){
//        this.accountService = accountService;
//    }

    @GetMapping("/get-all")//api: localhost:8080/api/v1/account/get-all
    public List<Account> getAll(){
        return accountService.getAll();
    }

    @PostMapping("/search")
    public Page<Account> search(@RequestBody SearchAccountReq req){
        return accountService.search(req);
    }
    @GetMapping("/{id}")
    public Account getById(@PathVariable int id){
        return accountService.getById(id);
    }

    @GetMapping("/find-by-username")
    public Account findByUsername(@RequestParam String username){

        return accountService.findByUsername(username);
    }


    @PostMapping("/create")
    public Account create(@RequestBody AccountCreateDto accountCreateDto){
        return accountService.create(accountCreateDto);
    }
    @DeleteMapping ("/{id}")
    public String delete(@PathVariable int id){
        try {
            accountService.delete(id);
            return "delete success";
        } catch (Exception e){
            return e.getMessage();
        }
    }
    @PutMapping("/edit")
    public Account edit(@RequestBody AccountEditDto accountEditDto){

        return accountService.edit(accountEditDto);
    }


}
