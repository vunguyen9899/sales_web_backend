package com.vti.shoppera70backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppeRa70BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppeRa70BackendApplication.class, args);
    }

}
