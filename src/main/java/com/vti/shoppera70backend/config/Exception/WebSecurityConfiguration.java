package com.vti.shoppera70backend.config.Exception;

import com.vti.shoppera70backend.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private AccountService accountService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountService).
                passwordEncoder(passwordEncoder);
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()

                .antMatchers("api/not-authenticated","/api/v1/account/create","/api/v1/auth/register",
                        "/api/v1/auth/login-jwt","/api/v1/auth/**","/api/v1/product/search","/api/v1/account/get-all",
                        "/api/v1/account/get-all").permitAll()



                .antMatchers(HttpMethod.DELETE,"/api/v1/product/{id}").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.POST,"/api/v1/product/create").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/v1/product/edit").hasAuthority("ADMIN")




                .antMatchers("/api/admin-or-user","/api/v1/order/*","/api/v1/order/search","/api/v1/order/pay","/api/v1/order/cancel").hasAnyAuthority("ADMIN", "CUSTOMER")


                .anyRequest().authenticated()


                .and().httpBasic()

                .and().cors().and().csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/swagger-ui/**")
                .antMatchers("/swagger-resources/**")
                .antMatchers("/v3/api-docs/**");
    }
}
