package com.vti.shoppera70backend.config.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;


@ControllerAdvice
    public class ControllerExceptionHandler {


        @ExceptionHandler(CustomerException.class)
        public ResponseEntity<CustomerException> catchExceptionCustom(CustomerException exception, HttpServletRequest request){
            exception.setPath(request.getRequestURI());
            return ResponseEntity.status(exception.getStatus())
                    .body(exception);
        }



        @ExceptionHandler(BindException.class)
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        public ResponseEntity<CustomerException> handleBindException(BindException e, HttpServletRequest request) {
            String errorMessage = "";
            if (e.getBindingResult().hasErrors()){
                for(int i=0;i< e.getBindingResult().getAllErrors().size();i++){
                    errorMessage += e.getBindingResult().getAllErrors().get(i).getDefaultMessage();
                    errorMessage += (i==e.getBindingResult().getAllErrors().size()-1) ? "." : ", ";
                }
            }
            CustomerException appException= new CustomerException(errorMessage, 400, request.getRequestURI());
            return new ResponseEntity<>(appException, HttpStatus.valueOf(appException.getStatus()));
        }


        @ExceptionHandler(Exception.class)
        public ResponseEntity<CustomerException> catchExceptionGlobal(Exception exception, HttpServletRequest request){
            CustomerException appException = new CustomerException(exception.getMessage(), 500, request.getRequestURI());
            appException.setPath(request.getRequestURI());
            return ResponseEntity.status(appException.getStatus())
                    .body(appException);
        }




    }


