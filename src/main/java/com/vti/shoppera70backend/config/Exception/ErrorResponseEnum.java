package com.vti.shoppera70backend.config.Exception;

public enum ErrorResponseEnum {
    NOT_FOUND_PRODUCT(404, "Không tìm thấy sản phẩm"),
    NOT_FOUND_ACCOUNT(404, "Không tìm thấy người dùng"),
    USERNAME_EXISTED(400, "Username đã tồn tại!"),
    LOGIN_FAIL_USERNAME(401, "Username sai"),
    LOGIN_FAIL_PASSWORD(401, "PASSWORD sai");



    public final int status;
    public final String message;
    ErrorResponseEnum(int status, String message) {
        this.status = status;
        this.message = message;
    }

}
