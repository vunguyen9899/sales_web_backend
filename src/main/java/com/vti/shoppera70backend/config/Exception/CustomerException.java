package com.vti.shoppera70backend.config.Exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@JsonIgnoreProperties({"stackTrace","cause","suppressed","localizedMessage"})
public class CustomerException extends RuntimeException {
    private Instant timestamp;
    private String message;
    private String path;
    private int status;

    public CustomerException(int status,String message ) {
        this.timestamp = Instant.now();
        this.status = status;
        this.message = message;
    }

    public CustomerException(ErrorResponseEnum notFoundProduct) {
        this.timestamp = Instant.now();
        this.status = notFoundProduct.status;
        this.message = notFoundProduct.message;
    }

    public CustomerException(String errorMessage, int status, String path) {
        this.timestamp = Instant.now();
        this.status = status;
        this.message = errorMessage;
        this.path = path;
    }
}
